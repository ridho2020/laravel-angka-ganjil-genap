<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GanjilGenapController extends Controller
{
    public function index()
    {
        return view('ganjilgenap');
    }

    public function operasi(Request $request)
    {
        $angka1 = $request->input('angka1');
        $angka2 = $request->input('angka2');
        $hasil = [];

        for ($i = $angka1; $i < $angka2; $i++) {
            if ($i % 2 == 0) {
                array_push($hasil, $i . "Bilangan Genap");
            } else {
                array_push($hasil,  $i . "Bilangan Ganjil");
            }
        }
        return view('ganjilgenap', [
            'hasil' => $hasil
        ]);
    }
}
